<?php
require(APPPATH.'libraries/REST_Controller.php');
require(APPPATH.'libraries/JWT.php');

use \Firebase\JWT\JWT;
use Restserver\Libraries\REST_Controller;

class Api extends REST_Controller
{
	private $limit = 10;
	private $_payload;

	public function __construct($config = "rest")
	{
		parent::__construct($config);
		$this->load->database();

		$token = $this->input->get_request_header('Authorization');

		if(!$token) {
			$output = array("Error" => "Access Denied");
			$this->response($output, REST_Controller::HTTP_UNAUTHORIZED);
			die();
		}
		try {
			$this->_payload = JWT::decode($token, $this->config->item('jwt_secret_key'),array('HS256'));
		} catch (Exception $ex) {
			$output = array("Error" => $ex->getMessage());
			$this->response($output, REST_Controller::HTTP_UNAUTHORIZED);
			die();
		}
	}

	public function currency_get()
	{
	// respond with information about a currency
		$sql = "SELECT name, rate FROM currency WHERE id = ".$this->get('id').";";
		$query = $this->db->query($sql);
		$row = $query->row_array();
		$this->response($row);
	}

	public function currencies_get()
	{
	// respond with information about currencies
		$count = $this->db->count_all('currency');
		$pages = ceil($count/$this->limit);
		if (!$this->get('page')) {
			$sql = "SELECT name, rate FROM currency;";
		} elseif($this->get('page') < $pages) {
			$sql = "SELECT name, rate FROM currency LIMIT ".$this->limit." OFFSET ".intval($this->get('page'))*$this->limit.";";
		} else {
			$this->response("Cannot found page", 500);
			return;
		}

		$query = $this->db->query($sql);
		$rows = $query->result_array();
		$this->response($rows);
	}
}
