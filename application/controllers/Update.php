<?php  if ( ! defined('BASEPATH')) exit("No direct script access allowed");

class Update extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->input->is_cli_request()
		or exit("Execute via command line: php index.php update");

		$this->load->database();
	}

	public function index()
	{
		$xml = simplexml_load_file('http://www.cbr.ru/scripts/XML_daily.asp');
		$currencies = '';
		foreach($xml as $currency) {
			$currencies .= 	'("'.$currency->CharCode.'",'.str_replace(',','.',$currency->Value).'),';
		}
		$sql = "TRUNCATE TABLE currency;";
		$this->db->query($sql);
		$sql = "INSERT INTO currency (name, rate) VALUES ".substr($currencies,0,-1).";";
		$this->db->query($sql);
		echo "Updated: ". $this->db->affected_rows();
	}
}
