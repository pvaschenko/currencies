<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_currency extends CI_Migration
{

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '3',
			),
			'rate' => array(
				'type' => 'FLOAT',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('currency');
	}

	public function down()
	{
		$this->dbforge->drop_table('currency');
	}
}
