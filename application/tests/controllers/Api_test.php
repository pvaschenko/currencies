<?php

class Api_test extends TestCase
{
	private $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1NzMxNTg5MDksImV4cCI6MTYwNDY5NDkwOSwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZXQiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJSb2xlIjpbIk1hbmFnZXIiLCJQcm9qZWN0IEFkbWluaXN0cmF0b3IiXX0.FVGh798jyp6bYMX3fDc5vqslfOH8FMo9M4cVflWD31I';

	public function test_currency()
	{
		$this->request->setHeader('Authorization',$this->token);
		$output = $this->request('GET', 'api/currency?id=1');
		$this->assertContains('name', $output);
	}

	public function test_currencies()
	{
		$this->request->setHeader('Authorization',$this->token);
		$output = $this->request('GET', 'api/currencies');
		$this->assertContains('name', $output);

		$output = $this->request('GET', 'api/currencies?page=0');
		$this->assertContains('name', $output);

		$output = $this->request('GET', 'api/currencies?page=1');
		$this->assertContains('name', $output);

		$output = $this->request('GET', 'api/currencies?page=2');
		$this->assertContains('name', $output);

		$output = $this->request('GET', 'api/currencies?page=3');
		$this->assertContains('name', $output);
	}

}
