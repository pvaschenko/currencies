1.Установка библиотек:

    composer install

2.Cоздание таблицы:

    php index.php migrate
 
3.Обновление валют с помощью консольной команды:

    php index.php update
    
4.Запуск тестов:

    cd \application\tests
    ..\..\vendor\bin\phpunit
